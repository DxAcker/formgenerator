let router = require("express").Router();
const testForm = require("./testFormModel");
const express = require("express");
const jsonParser = express.json();

router.get("/forms", (req, res) => {
  return testForm.find((err, forms) => {
    if (!err) return res.send(forms);
    else {
      res.statusCode = 500;
      log.error("Internal error(%d): %s", res.statusCode, err.message);
      return res.send({ error: "Server error" });
    }
  });
});

router.post("/forms", jsonParser, (req, res) => {
  let form = new testForm({
    title: req.body.title,
    components: req.body.components,
  });

  form.save((err) => {
    if (!err) return res.send({ status: "OK", form: form });
    else {
      console.log(err);
      res.statusCode = 500;
      res.send({ error: "Server error" });
    }
  });
});

module.exports = router;
