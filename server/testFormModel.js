const mongoose = require("mongoose");

const TestForm = mongoose.Schema({
  title: String,
  components: [{}],
});

module.exports = mongoose.model("TestForm", TestForm);
