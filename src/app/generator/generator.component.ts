import { TestField, Choice, TestForm } from './../form-controller.service';
import { Component, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef, ViewRef } from '@angular/core';
import {TestCheckboxComponent} from "../test-checkbox/test-checkbox.component";
import {TestInputComponent} from "../test-input/test-input.component";
import {TestNumberComponent} from "../test-number/test-number.component";
import {TestSelectComponent} from "../test-select/test-select.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import { FormControllerService } from '../form-controller.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.scss']
})
export class GeneratorComponent implements OnInit {

  // @ts-ignore
  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef

  public testForm: TestForm = {
    title: '',
    components: []
  }

  components = []

  testCheckbox = TestCheckboxComponent
  testInput = TestInputComponent
  testNumber = TestNumberComponent
  testSelect = TestSelectComponent

  // Variables for creating component
  testField: TestField = {
    label: '',
    description: '',
    placeholder: '',
    required: false,
    choices: [],
    componentClass: null,
    isIncludeCheckAll: false
  }
  choicesText: string = ''

  forms: TestForm[] = []

  constructor(private http: HttpClient, private componentFactoryResolver: ComponentFactoryResolver, private ngbModal: NgbModal, private formControllService: FormControllerService) {
    this.formControllService.removeComponentObs$.subscribe(
      componentId => {
        console.log('Generator has got a message for remove component ' + componentId)
        this.removeComponent(componentId)
      }
    )

    this.formControllService.moveComponentUp$.subscribe(
      componentId => {
        console.log('Generator has got a message for move component up ' + componentId)
        this.moveComponentUp(componentId)
      }
    )

    this.formControllService.moveComponentDown$.subscribe(
      componentId => {
        console.log('Generator has got a message for move component up ' + componentId)
        this.moveComponentDown(componentId)
      }
    )
  }

  addComponent(componentClass: any) {
    console.log(componentClass)
    // Create component dynamically inside the ng-template
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentClass)
    const component = this.container.createComponent(componentFactory)

    let choicesArray = this.choicesText.split(',')

    let choices: Choice[] = []
    choicesArray.forEach((choice, idx) => {
      choices.push(
        {id: idx, title: choice, active: false}
      )
    })

    this.testField.choices = choices
    if (componentClass == TestInputComponent) this.testField.componentClass = 'TestInputComponent'
    else if (componentClass == TestCheckboxComponent) this.testField.componentClass = 'TestCheckboxComponent'
    else if (componentClass == TestNumberComponent) this.testField.componentClass = 'TestNumberComponent'
    else if (componentClass == TestSelectComponent) this.testField.componentClass = 'TestSelectComponent'
    this.formControllService.initTestField(this.testField)
    this.testForm.components.push(this.testField)

    // Empty all
    this.choicesText = ''
    this.testField = {
      label: '',
      description: '',
      placeholder: '',
      required: false,
      choices: [],
      componentClass: null,
      isIncludeCheckAll: false
    }

    // @ts-ignore
    this.components.push(component);
  }

  removeComponent(componentId: number) {
    // Find the component
    const component = this.components.find((component: ComponentRef<any>) => component.instance.componentId === componentId);
    // @ts-ignore
    const componentIndex = this.components.indexOf(component)

    if (componentIndex !== -1) {
      this.container.remove(componentIndex)
      this.components.splice(componentIndex, 1)
      this.testForm.components.splice(componentIndex, 1)
    }
  }

  moveComponentUp(componentId: number) {

    // Find the component
    // @ts-ignore
    const component = this.components.find((component) => component.instance.componentId === componentId);

    // @ts-ignore
    const componentIndex = this.components.indexOf(component);

    if (componentIndex !== -1 && componentIndex > 0) {
      // Get ViewRef object
      // @ts-ignore
      const componentViewRef: ViewRef = component['hostView']

      this.container.move(componentViewRef, componentIndex - 1);

      // Swap components in array
      const temp = this.components[componentIndex - 1]
      this.components[componentIndex - 1] = this.components[componentIndex]
      this.components[componentIndex] = temp

      const ttemp = this.testForm.components[componentIndex - 1]
      this.testForm.components[componentIndex - 1] = this.testForm.components[componentIndex]
      this.testForm.components[componentIndex] = ttemp
    }
  }

  moveComponentDown(componentId: number) {
    // Find the component
    // @ts-ignore
    const component = this.components.find((component) => component.instance.componentId === componentId);
    // @ts-ignore
    const componentIndex = this.components.indexOf(component);

    if (componentIndex !== -1 && componentIndex < this.components.length - 1) {
      // Get ViewRef object
      // @ts-ignore
      const componentViewRef: ViewRef = component['hostView']

      this.container.move(componentViewRef, componentIndex + 1);

      // Swap components in array
      const temp = this.components[componentIndex + 1]
      this.components[componentIndex + 1] = this.components[componentIndex]
      this.components[componentIndex] = temp

      const ttemp = this.testForm.components[componentIndex + 1]
      this.testForm.components[componentIndex + 1] = this.testForm.components[componentIndex]
      this.testForm.components[componentIndex] = ttemp
    }

  }

  openModal(content: any): void {
    console.log(content)
    if (content._declarationTContainer.localNames[0] === "loadform")
      this.http.get("http://localhost:3000/api/forms").subscribe(value => {
        this.forms = value as TestForm[]
      })
    this.ngbModal.open(content)
  }

  saveForm() {
    console.log(this.testForm)
    this.http.post("http://localhost:3000/api/forms", this.testForm).subscribe(() => {})
  }

  loadForm(form: TestForm) {
    console.log(form)
    this.container.clear()
    this.components = []
    this.testForm = form
    form.components.forEach(field => {
      let componentClass
      if (field.componentClass === 'TestInputComponent') componentClass = this.testInput
      else if (field.componentClass === 'TestCheckboxComponent') componentClass = this.testCheckbox
      else if (field.componentClass === 'TestSelectComponent') componentClass = this.testSelect
      else if (field.componentClass === 'TestNumberComponent') componentClass = this.testNumber
      // Create component dynamically inside the ng-template
      let componentFactory
      if (componentClass) {
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentClass as any)
        const component = this.container.createComponent(componentFactory)
        this.formControllService.initTestField(field)
        // @ts-ignore
        this.components.push(component)
      }

    })
  }

  ngOnInit(): void {

  }

}

