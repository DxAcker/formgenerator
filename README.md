# FormGenerator

Проект был сгенерирован с помощью [Angular CLI](https://github.com/angular/angular-cli) version 12.1.0.

## Development server

Запустите `ng serve` и `node server/server.js` для дев-сервера. Перейдите на `http://localhost:4200/`. Также, для работы сервера понадобится MongoDB.

## Build

Запустите `ng build`, чтобы построить проект. Сжатые компоненты будут находиться в `dist/` директории.

## Credits

Проект писал `@dxack`. Дизайн-лист, а также многие требования ТЗ были учтены.
